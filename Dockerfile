ARG IMAGE_TAG=latest
FROM docker.io/python:${IMAGE_TAG}

ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

ENV PATH="/root/.cargo/bin:${PATH}"

ARG DEBIAN_FRONTEND=noninteractive

COPY requirements.txt .

RUN apt-get update && \
    apt-get full-upgrade -y && \
    apt-get install curl build-essential -y && \
    pip3 install uv && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y && \
    uv pip install --system --no-cache-dir --upgrade -r requirements.txt && \
    rustup self uninstall -y && \
    rm -rf /root/.cargo && \
    apt-get purge -y build-essential && \
    apt-get autopurge -y && \
    rm -rf /var/lib/apt/lists/*
