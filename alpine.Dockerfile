ARG IMAGE_TAG=alpine
FROM docker.io/python:${IMAGE_TAG}

ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

ENV PATH="/root/.cargo/bin:${PATH}"

COPY requirements.txt .

RUN apk upgrade --no-cache && \
    apk add --no-cache curl libgcc findutils build-base libffi-dev rust cargo && \
    pip3 install uv --no-cache-dir && \
    uv pip install --system --no-cache-dir --upgrade -r requirements.txt && \
    apk del --no-cache libgcc build-base libffi-dev rust cargo && \
    rm -rf /root/.cargo
